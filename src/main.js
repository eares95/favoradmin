import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy, {
  defaultIconPack: 'fas'
})

Vue.mixin({
  data: function () {
    const loader = false
    const url = 'https://favorapi.com/api'
    return {
      loader: loader,
      baseUrl: url
    }
  }
})
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
