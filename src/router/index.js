import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/packages'
  },
  {
    path: '/approves',
    name: 'Approves',
    component: () => import(/* webpackChunkName: "approves" */ '../views/Approves.vue')
  },
  {
    path: '/businesses',
    name: 'Businesses',
    component: () => import(/* webpackChunkName: "businesses" */ '../views/Businesses.vue')
  },
  {
    path: '/packages',
    name: 'Packages',
    component: () => import(/* webpackChunkName: "packages" */ '../views/Packages.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: 'is-active'
})

export default router
